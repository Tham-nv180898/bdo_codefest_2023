package bdoteam.codefest.service.model

import bdoteam.codefest.service.socket.data.MapInfo
import bdoteam.codefest.service.socket.data.Player
import bdoteam.codefest.service.socket.data.Size
import bdoteam.codefest.utils.Constant.Constants

data class Match (
    val ownPosition: Pair<Int, Int>,
    val playerSpeed: Int,
    val opponentPosition: Pair<Int, Int>,
    val roads: List<Pair<Int, Int>>,
    val balks: List<Pair<Int, Int>>,
    val walls: List<Pair<Int, Int>>,
    val teleportGates: List<Pair<Int, Int>>,
    val quantityPlace: List<Pair<Int, Int>>,
    val dragonEgeGsts: List<Pair<Int, Int>>,
    val bombs: List<Pair<Int, Int>>,
    val map: List<List<Int>>,
    val size: Size
)


fun MapInfo.getOwnPosition(): Pair<Int, Int> {
    val player: Player = this.players.first { player ->
        player.id == Constants.KEY_TEAM
    }
    return Pair(player.currentPosition.row, player.currentPosition.col)
}

fun MapInfo.getSpeed(): Int {
    val player: Player = this.players.first { player ->
        player.id == Constants.KEY_TEAM
    }
    return player.speed
}

fun MapInfo.getOpponentPosition(): Pair<Int, Int>{
    val player: Player = this.players.first { player ->
        player.id != Constants.KEY_TEAM
    }
    return Pair(player.currentPosition.row, player.currentPosition.col)
}

fun MapInfo.analysisMatch(): Match {
    val roads: MutableList<Pair<Int, Int>> = mutableListOf()
    val walls: MutableList<Pair<Int, Int>> = mutableListOf()
    val balks: MutableList<Pair<Int, Int>> = mutableListOf()
    val teleportGates: MutableList<Pair<Int, Int>> = mutableListOf()
    val quantityPlace: MutableList<Pair<Int, Int>> = mutableListOf()
    val dragonEgeGsts: MutableList<Pair<Int, Int>> = mutableListOf()

    for (i in (0 until this.size.rows)) {
        for (j in (0 until this.size.cols)) {
            val position = Pair(i, j)
            when (this.map[i][j]) {
                0 -> {
                    roads.add(position)
                }

                1 -> {
                    walls.add(position)
                }

                2 -> {
                    balks.add(position)
                }

                3 -> {
                    teleportGates.add(position)
                }

                4 -> {
                    quantityPlace.add(position)
                }
                5 -> {
                    dragonEgeGsts.add(position)
                }

            }
        }
    }
    val ownPosition = getOwnPosition()
    val opponentPosition = getOpponentPosition()
    val bomb = this.bombs.map {
        Pair(it.col, it.row)
    }
    val speed = getSpeed()
    return Match(ownPosition, speed, opponentPosition, roads, balks, walls, teleportGates, quantityPlace, dragonEgeGsts, bomb, map, size)
}

