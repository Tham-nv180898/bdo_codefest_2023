package bdoteam.codefest.service.caculator

import kotlin.math.abs

open class BaseAlgorithm {
    fun heuristic(a: Pair<Int, Int>, b: Pair<Int, Int>): Int {
        return abs(a.first - b.first) + abs(a.second - b.second)
    }
}