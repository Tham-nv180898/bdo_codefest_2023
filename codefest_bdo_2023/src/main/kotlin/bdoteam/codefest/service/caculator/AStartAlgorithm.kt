package bdoteam.codefest.service.caculator

import bdoteam.codefest.service.socket.data.Node
import java.util.*


class AStartAlgorithm : BaseAlgorithm() {

    fun aStarSearch(map: List<List<Int>>, start: Pair<Int, Int>, end: Pair<Int, Int>): List<Pair<Int, Int>>? {
        val openSet = PriorityQueue<Node>(compareBy { it.cost + it.heuristic })
        val closedSet = mutableSetOf<Pair<Int, Int>>()
        val startNode = Node(start.first, start.second, 0, heuristic(start, end), null)
        openSet.add(startNode)
        while (openSet.isNotEmpty()) {
            val current = openSet.poll()
            if (current.x == end.first && current.y == end.second) {
                return reconstructPath(current)
            }
            val neighbors = getNeighbors(current, map)
            for (neighbor in neighbors) {
                if (neighbor in closedSet) {
                    continue
                }

                val tentativeCost = current.cost + 1
                val neighborNode =
                    Node(neighbor.first, neighbor.second, tentativeCost, heuristic(neighbor, end), current)

                if (!openSet.contains(neighborNode)) {
                    openSet.add(neighborNode)
                }
            }
            closedSet.add(Pair(current.x, current.y))
        }
        return null
    }

    private fun reconstructPath(node: Node): List<Pair<Int, Int>> {
        val path = mutableListOf<Pair<Int, Int>>()
        var current: Node? = node

        while (current != null) {
            path.add(0, Pair(current.x, current.y))
            current = current.parent
        }

        return path
    }

    private fun getNeighbors(node: Node, map: List<List<Int>>): List<Pair<Int, Int>> {
        val neighbors = mutableListOf<Pair<Int, Int>>()
        val dx = intArrayOf(-1, 1, 0, 0)
        val dy = intArrayOf(0, 0, -1, 1)

        for (i in 0 until 4) {
            val newX = node.x + dx[i]
            val newY = node.y + dy[i]

            if (newX >= 0 && newX < map.size && newY >= 0 && newY < map[0].size && map[newX][newY] == 0) {
                neighbors.add(Pair(newX, newY))
            }
        }

        return neighbors
    }
}