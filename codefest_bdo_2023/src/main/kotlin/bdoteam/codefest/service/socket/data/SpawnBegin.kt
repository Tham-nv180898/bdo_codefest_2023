package bdoteam.codefest.service.socket.data

data class SpawnBegin(
    val col: Int,
    val row: Int
)