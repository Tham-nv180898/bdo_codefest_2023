package bdoteam.codefest.service.socket

class ClientConfig {
    class PLAYER {
        object OUTGOING {
            const val JOIN_GAME = "join game"
            const val DRIVE_PLAYER = "drive player"
        }

        object INCOMMING {
            const val JOIN_GAME = "join game"
            const val TICKTACK_PLAYER = "ticktack player"
            const val DRIVE_PLAYER = "drive player"
        }
    }
}


