package bdoteam.codefest.service.socket.data

data class CurrentPosition(
    val col: Int,
    val row: Int
)