package bdoteam.codefest.service.socket.data

data class Driver(
    val player_id: String,
    val direction: String
)
