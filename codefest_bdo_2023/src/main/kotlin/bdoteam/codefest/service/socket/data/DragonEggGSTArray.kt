package bdoteam.codefest.service.socket.data

data class DragonEggGSTArray(
    val col: Int,
    val id: String,
    val row: Int
)