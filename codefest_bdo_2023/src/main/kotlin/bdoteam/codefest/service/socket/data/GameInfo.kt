package bdoteam.codefest.service.socket.data

import com.google.gson.annotations.SerializedName

data class GameInfo(
    val gameRemainTime: Int,
    val id: Int,
    val map_info: MapInfo,
    val tag: String,
    val timestamp: Long
)