package bdoteam.codefest.service.socket.data

data class Size(
    val cols: Int,
    val rows: Int
)