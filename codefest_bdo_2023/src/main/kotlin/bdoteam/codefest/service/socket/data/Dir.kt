package bdoteam.codefest.service.socket.data

import com.google.gson.Gson

class Dir() {
    private var direction: String? = null
    fun setDirection(dir: String) {
        direction = dir
    }

    override fun toString(): String {
        return Gson().toJson(this)
    }

    companion object {
        const val LEFT = "1"
        const val RIGHT = "2"
        const val UP = "3"
        const val DOWN = "4"
        const val DROP_BOMB = "b"
        const val INVALID = ""
        const val STOP = "x"

        val MOVE_TO_STRING: HashMap<String?, String?> = object : HashMap<String?, String?>() {
            init {
                put(UP, "UP")
                put(LEFT, "LEFT")
                put(DOWN, "DOWN")
                put(RIGHT, "RIGHT")
                put(INVALID, "INVALID")
                put(DROP_BOMB, "DROP BOMB")
            }
        }

    }
}

