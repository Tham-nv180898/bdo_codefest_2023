package bdoteam.codefest.service.socket.data

import com.google.gson.annotations.SerializedName

data class Game(
    @SerializedName("player_id")
    val playerId: String,
    @SerializedName("game_id")
    val gameId: String
)
