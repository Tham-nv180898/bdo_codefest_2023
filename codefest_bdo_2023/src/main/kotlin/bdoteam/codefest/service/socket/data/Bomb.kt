package bdoteam.codefest.service.socket.data

import androidx.compose.ui.graphics.Color

data class Bomb(
    val row: Int,
    val col: Int
)
