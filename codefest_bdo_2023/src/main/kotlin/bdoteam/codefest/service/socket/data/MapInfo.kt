package bdoteam.codefest.service.socket.data


data class MapInfo(
    val bombs: List<Bomb>,
    val dragonEggGSTArray: List<DragonEggGSTArray>,
    val gameStatus: Any,
    val map: List<List<Int>>,
    val players: List<Player>,
    val size: Size,
    val spoils: List<Any>
)

