package bdoteam.codefest.service.socket.data

data class Node(val x: Int, val y: Int, val cost: Int, val heuristic: Int, val parent: Node?)
