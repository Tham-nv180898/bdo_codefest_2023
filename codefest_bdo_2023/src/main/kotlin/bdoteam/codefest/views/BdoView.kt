package bdoteam.codefest.views

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import bdoteam.codefest.controller.SocketController
import bdoteam.codefest.controller.model.ConnectionInfo
import bdoteam.codefest.utils.Constant.Constants

@Composable
@Preview
fun BdoView() {
    val gameIdValue = remember { mutableStateOf(TextFieldValue(text = Constants.KEY_MAP)) }
    val playIdValue = remember { mutableStateOf(TextFieldValue(text = Constants.KEY_TEAM)) }
    val urlValue = remember { mutableStateOf(TextFieldValue(text = Constants.URL)) }
    val mExpanded = remember { mutableStateOf(false) }
    val enableProxy = remember { mutableStateOf(false) }
    val enableDebugMode = remember { mutableStateOf(false) }
    val isUrlError = remember { mutableStateOf(false) }
    val isPlayerIdError = remember { mutableStateOf(false) }
    val isGameIdError = remember { mutableStateOf(false) }
    val enableRegisterButton = remember { mutableStateOf(true) }
    val enableStopButton = remember { mutableStateOf(false) }

    val socketController = SocketController()


    MaterialTheme {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.width(750.dp)
            ) {
                Text("", fontSize = 24.sp, fontWeight = FontWeight.Bold, modifier = Modifier.width(200.dp))
                Column {
                    IconButton(onClick = {
                        mExpanded.value = true
                    }) {
                        Icon(imageVector = Icons.Outlined.Settings, contentDescription = "Setting")
                    }
                    DropdownMenu(
                        expanded = mExpanded.value,
                        onDismissRequest = { mExpanded.value = false },
                        modifier = Modifier.height(100.dp)
                    ) {
                        DropdownMenuItem(onClick = {
                            mExpanded.value = false
                        }) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Text("Enable proxy")
                                Checkbox(enableProxy.value,
                                    onCheckedChange = { enableProxy.value = !enableProxy.value })
                            }
                        }
                        DropdownMenuItem(onClick = {
                            mExpanded.value = false
                        }) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Text("Debug mode")
                                Checkbox(enableDebugMode.value,
                                    onCheckedChange = { enableDebugMode.value = !enableDebugMode.value })
                            }
                        }
                    }
                }

            }
            Spacer(modifier = Modifier.height(10.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("URL: ", fontSize = 18.sp, fontWeight = FontWeight.Bold, modifier = Modifier.width(100.dp))
                Column {
                    OutlinedTextField(
                        value = urlValue.value, onValueChange = {
                            urlValue.value = it
                            isUrlError.value = false
                        }, modifier = Modifier.width(500.dp), isError = isUrlError.value
                    )
                    if (isUrlError.value) {
                        Text("Please input URL", color = Color.Red)
                    }
                }
            }
            Spacer(modifier = Modifier.height(10.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("Game ID: ", fontWeight = FontWeight.Bold, fontSize = 18.sp, modifier = Modifier.width(100.dp))
                Column {
                    OutlinedTextField(
                        value = gameIdValue.value,
                        onValueChange = {
                            gameIdValue.value = it
                            isGameIdError.value = false
                        },
                        modifier = Modifier.width(500.dp),
                        isError = isGameIdError.value,
                    )
                    if (isGameIdError.value) {
                        Text("Please input Game Id", color = Color.Red)
                    }
                }
            }
            Spacer(modifier = Modifier.height(10.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("Play ID:", fontSize = 18.sp, fontWeight = FontWeight.Bold, modifier = Modifier.width(100.dp))
                Column {
                    OutlinedTextField(
                        value = playIdValue.value, onValueChange = {
                            playIdValue.value = it
                            isGameIdError.value = false
                        }, modifier = Modifier.width(500.dp), isError = isPlayerIdError.value
                    )
                    if (isPlayerIdError.value) {
                        Text("Please input Player ID", color = Color.Red)
                    }
                }
            }
            Spacer(modifier = Modifier.height(15.dp))
            Button(
                onClick = {
                    if (urlValue.value.text.isEmpty()) {
                        isUrlError.value = true
                    }
                    if (playIdValue.value.text.isEmpty()) {
                        isPlayerIdError.value = true
                    }
                    if (gameIdValue.value.text.isEmpty()) {
                        isGameIdError.value = true
                    }
                    if (!isUrlError.value && !isGameIdError.value && !isPlayerIdError.value) {
                        val connectionInfo = ConnectionInfo(
                            url = urlValue.value.text.trim(),
                            playId = playIdValue.value.text.trim(),
                            gameId = gameIdValue.value.text.trim()
                        )
                        socketController.connectToServer(connectionInfo = connectionInfo,
                            errorCallBack = {
                            enableRegisterButton.value = true
                            enableStopButton.value = false
                        },
                            successCallBack = {
                                enableRegisterButton.value = false
                                enableStopButton.value = true
                            }
                        )
                    }
                },
                modifier = Modifier.width(600.dp).height(50.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF0009000)),
                enabled = enableRegisterButton.value
            ) {
                Text("Register", color = Color.White)
            }
            Spacer(modifier = Modifier.height(15.dp))
            Button(
                onClick = {
                    socketController.stopGame(disconnectCallback = {
                        enableRegisterButton.value = true
                        enableStopButton.value = false
                    })
                },
                modifier = Modifier.width(600.dp).height(50.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF880000)),
                enabled = enableStopButton.value
            ) {
                Text("Stop", color = Color.White)
            }
            Spacer(modifier = Modifier.height(10.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.width(600.dp)
            ) {
                Text("Message", fontSize = 16.sp, fontWeight = FontWeight.Normal, modifier = Modifier.width(100.dp))
                IconButton(onClick = {}) {
                    Icon(imageVector = Icons.Outlined.Delete, contentDescription = "Clear")
                }
            }
            OutlinedTextField(
                value = playIdValue.value,
                onValueChange = { playIdValue.value = it },
                modifier = Modifier.width(600.dp).height(200.dp),
                enabled = false
            )
            Spacer(modifier = Modifier.height(50.dp))
        }
    }
}