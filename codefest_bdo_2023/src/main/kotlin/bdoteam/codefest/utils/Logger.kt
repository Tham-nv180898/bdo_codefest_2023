package bdoteam.codefest.utils

object Logger {
    private var isShowLog = true

    fun updateDebugMode(debugMode: Boolean) {
        isShowLog = debugMode
    }

    fun println(log: String?) {
        if (isShowLog) {
            kotlin.io.println(log)
        }
    }
}