package bdoteam.codefest.utils

import io.socket.client.IO
import io.socket.client.Socket
import okhttp3.*
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URISyntaxException
import java.util.concurrent.TimeUnit

object SocketUtils {
    private const val TIMEOUT_IN_MINUTES = 1
    private const val HOST = "hl-proxyb"
    private const val PORT = 8080
    private const val USER_NAME = "thamnv2"
    private const val PASSWORD = "Helloword***"

    fun init(url: String, isEnableProxy: Boolean): Socket? {
        val okHttpClient: OkHttpClient = getHttpClientBuilder(isEnableProxy = isEnableProxy)
        IO.setDefaultOkHttpCallFactory(okHttpClient)
        IO.setDefaultOkHttpWebSocketFactory(okHttpClient)
        return try {
            IO.socket(url)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
            null
        }
    }

    private fun getHttpClientBuilder(isEnableProxy: Boolean = false): OkHttpClient {
        val clientBuilder =
            OkHttpClient.Builder().connectTimeout(TIMEOUT_IN_MINUTES.toLong(), TimeUnit.MINUTES)
                .writeTimeout(TIMEOUT_IN_MINUTES.toLong(), TimeUnit.MINUTES)
                .readTimeout(TIMEOUT_IN_MINUTES.toLong(), TimeUnit.MINUTES)
        if (isEnableProxy) {
            val host = HOST
            val port = PORT
            val username = USER_NAME
            val password = PASSWORD
            clientBuilder.proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(host, port)))
            clientBuilder.proxyAuthenticator(ProxyAuthenticator(username, password))
        }
        return clientBuilder.build()
    }


    private class ProxyAuthenticator(private val username: String, private val password: String) : Authenticator {
        override fun authenticate(route: Route?, response: Response): Request? {
            val credential = Credentials.basic(username, password)
            return response.request().newBuilder()
                .header("Proxy-Authorization", credential)
                .build()
        }
    }

}