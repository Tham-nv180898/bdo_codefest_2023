package bdoteam.codefest.utils

import com.google.gson.GsonBuilder

fun <T> T.toJsonString(): String{
    val gson = GsonBuilder().disableHtmlEscaping().create()
    return gson.toJson(this)
}