package bdoteam.codefest

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject
import bdoteam.codefest.utils.SocketUtils
import bdoteam.codefest.views.BdoView


fun main() = application {
    Window(onCloseRequest = ::exitApplication, title = "BDO CODEFEST 2023", state = rememberWindowState(height = 750.dp),) {
        BdoView()
    }
}
