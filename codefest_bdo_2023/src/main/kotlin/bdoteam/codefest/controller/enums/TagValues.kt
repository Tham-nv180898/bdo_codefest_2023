package bdoteam.codefest.controller.enums

enum class PlayerTag(val value: String){
    MOVING_BANNED("player:moving-banned"),
    START_MOVING("player:start-moving"),
    STOP_MOVING("player:stop-moving"),
    BE_ISOLATED("player:be-isolated"),
    BACK_TO_PLAYGROUND("player:back-to-playground"),
    PICK_SPOIL("pick_spoil")
}

enum class BombTag(val value: String){
    EXPLOSED("bomb:explosed"),
    SETUP("bomb:setup")
}

enum class GameTag(val value: String){
    START_GAME("start-game"),
    UPDATE_DATA("update-data")
}