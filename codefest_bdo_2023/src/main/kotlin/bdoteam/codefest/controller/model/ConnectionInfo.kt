package bdoteam.codefest.controller.model

data class ConnectionInfo(
    val url: String,
    var gameId: String,
    var playId: String,
    var isEnableProxy: Boolean = false
)
