package bdoteam.codefest.controller

import bdoteam.codefest.service.caculator.AStartAlgorithm
import bdoteam.codefest.service.model.Match
import java.util.LinkedList
import java.util.Queue
import kotlin.math.abs

class Bomberman() {
    private val aStar = AStartAlgorithm()
    private val pendingActions: Queue<String> = LinkedList()
    private var targetPosition: Pair<Int, Int> = Pair(0, 0)
    fun nextMove(match: Match, isRestartSearch: Boolean = true): String {
        // This method is called each time bomberman is required to choose an action
        val emptyTileSurroundingTiles = getEmptyTilesSurroundingBalks(match)
        val bombs = match.bombs
        println("list bomb: $bombs")
        return when {
            (pendingActions.isNotEmpty()) -> {
                val action = pendingActions.poll()
                val nextPosition = when (action) {
                    "3" -> Pair(match.ownPosition.first - 1, match.ownPosition.second)
                    "1" -> Pair(match.ownPosition.first, match.ownPosition.second - 1)
                    "4" -> Pair(match.ownPosition.first + 1, match.ownPosition.second)
                    "2" -> Pair(match.ownPosition.first, match.ownPosition.second - 1)
                    else -> match.opponentPosition
                }
                if (nextPosition in bombs) {
                    pendingActions.clear()
                    return "x"
                }
                val isAroundPositionBalks = listOf(
                    Pair(targetPosition.first + 1, targetPosition.second),
                    Pair(targetPosition.first - 1, targetPosition.second),
                    Pair(targetPosition.first, targetPosition.second + 1),
                    Pair(targetPosition.first, targetPosition.second - 1)
                ).filter { it.first >= 0 && it.first < match.map.size && it.second >= 0 && it.second < match.map[0].size }
                    .map { match.map[it.first][it.second] }.contains(2)

                if (!isAroundPositionBalks) {
                    pendingActions.clear()
                    return "x"
                }

                println("continue action: $action")
                action
            }

            emptyTileSurroundingTiles.isNotEmpty() -> {
                for (endPoint in emptyTileSurroundingTiles.sortedBy { abs(it.first - match.ownPosition.first) + abs(it.second - match.ownPosition.second) }) {
                    val path = aStar.aStarSearch(match.map, match.ownPosition, endPoint)
                    if (path != null) {
                        path.addActionsToMove()
                        // drop bomb
                        targetPosition = endPoint
                        dropBomb(endPoint, match)
                        println("pending actions: $pendingActions")
                        break
                    }
                }
                if (pendingActions.isNotEmpty()) {
                    val firstAction = pendingActions.poll()
                    println("first action: $firstAction")
                    firstAction
                } else {
                    "x"
                }
            }

            else -> {
                "x"
            }
        }
    }

    private fun dropBomb(tile: Pair<Int, Int>, match: Match) {
        pendingActions.add("b")
        val safeArea = listOf(
            Pair(tile.first - 1, tile.second + 1),
            Pair(tile.first - 2, tile.second + 1),
            Pair(tile.first - 3, tile.second + 1),
            Pair(tile.first - 4, tile.second + 1),
            Pair(tile.first - 5, tile.second + 1),

            Pair(tile.first - 1, tile.second - 1),
            Pair(tile.first - 2, tile.second - 1),
            Pair(tile.first - 3, tile.second - 1),
            Pair(tile.first - 4, tile.second - 1),
            Pair(tile.first - 5, tile.second - 1),

            Pair(tile.first + 1, tile.second - 1),
            Pair(tile.first + 2, tile.second - 1),
            Pair(tile.first + 3, tile.second - 1),
            Pair(tile.first + 4, tile.second - 1),
            Pair(tile.first + 5, tile.second - 1),

            Pair(tile.first + 1, tile.second + 1),
            Pair(tile.first + 2, tile.second + 1),
            Pair(tile.first + 3, tile.second + 1),
            Pair(tile.first + 4, tile.second + 1),
            Pair(tile.first + 5, tile.second + 1),

            Pair(tile.first - 1, tile.second - 2),
            Pair(tile.first - 1, tile.second - 3),
            Pair(tile.first - 1, tile.second - 4),
            Pair(tile.first - 1, tile.second - 5),

            Pair(tile.first + 1, tile.second - 2),
            Pair(tile.first + 1, tile.second - 3),
            Pair(tile.first + 1, tile.second - 4),
            Pair(tile.first + 1, tile.second - 5),


            Pair(tile.first - 1, tile.second + 2),
            Pair(tile.first - 1, tile.second + 3),
            Pair(tile.first - 1, tile.second + 4),
            Pair(tile.first - 1, tile.second + 5),

            Pair(tile.first + 1, tile.second + 2),
            Pair(tile.first + 1, tile.second + 3),
            Pair(tile.first + 1, tile.second + 4),
            Pair(tile.first + 1, tile.second + 5)
        ).filter { it.first >= 0 && it.first < match.map.size && it.second >= 0 && it.second < match.map[0].size && match.map[it.first][it.second] == 0 }
            .sortedBy { abs(it.first - tile.first) + abs(it.second - tile.second) }
        var path: List<Pair<Int, Int>>? = null
        for (endPoint in safeArea) {
            path = aStar.aStarSearch(match.map, tile, endPoint)
            if (path != null) {
                break
            }
        }
        if (path != null) {
            var currentPosition = path[0]
            for (index in (1 until path.size)) {
                val action =
                    when (Pair(
                        path[index].first - currentPosition.first,
                        path[index].second - currentPosition.second
                    )) {
                        Pair(-1, 0) -> "3"
                        Pair(0, -1) -> "1"
                        Pair(1, 0) -> "4"
                        Pair(0, 1) -> "2"
                        else -> "x"
                    }
                pendingActions.add(action)
                currentPosition = path[index]
            }
            pendingActions.add("x")
            pendingActions.add("x")
        }
    }

    private fun getEmptyTilesSurroundingBalks(match: Match): List<Pair<Int, Int>> {
        val allEmptyTilesSurroundingBalks = mutableListOf<Pair<Int, Int>>()
        val dx = intArrayOf(-1, 1, 0, 0)
        val dy = intArrayOf(0, 0, -1, 1)
        match.balks.forEach { location ->
            for (i in 0 until 4) {
                val newX = location.first + dx[i]
                val newY = location.second + dy[i]

                if (newX >= 0 && newX < match.size.rows && newY >= 0 && newY < match.size.cols && match.map[newX][newY] == 0) {
                    allEmptyTilesSurroundingBalks.add(Pair(newX, newY))
                }
            }
        }
        return allEmptyTilesSurroundingBalks
    }

    private fun List<Pair<Int, Int>>.addActionsToMove() {
        var currentPosition = this[0]
        pendingActions.clear()
        for (index in (1 until this.size)) {
            val action =
                when (Pair(this[index].first - currentPosition.first, this[index].second - currentPosition.second)) {
                    Pair(-1, 0) -> "3"
                    Pair(0, -1) -> "1"
                    Pair(1, 0) -> "4"
                    Pair(0, 1) -> "2"
                    else -> "x"
                }
            pendingActions.add(action)
            currentPosition = this[index]
        }
    }

}