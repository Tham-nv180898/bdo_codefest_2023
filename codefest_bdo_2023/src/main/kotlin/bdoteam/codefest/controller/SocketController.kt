package bdoteam.codefest.controller

import bdoteam.codefest.controller.enums.PlayerTag
import bdoteam.codefest.controller.model.ConnectionInfo
import bdoteam.codefest.service.model.analysisMatch
import bdoteam.codefest.service.socket.ClientConfig
import bdoteam.codefest.service.socket.data.Dir
import bdoteam.codefest.service.socket.data.Driver
import bdoteam.codefest.service.socket.data.Game
import bdoteam.codefest.service.socket.data.GameInfo
import bdoteam.codefest.utils.Constant.Constants
import bdoteam.codefest.utils.Logger
import bdoteam.codefest.utils.SocketUtils
import bdoteam.codefest.utils.toJsonString
import com.google.gson.Gson
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONException
import org.json.JSONObject

class SocketController {
    private var mSocket: Socket? = null
    private var bomberman = Bomberman()
    private val dir = Dir()
    private var mIsHandle = false
    private var firstTime = true
    private var timeTakingProcessing: Long = 0
    private var lastTime: Long = 0
    private val mOnJoinGameListener = Emitter.Listener { objects ->
        mIsHandle = true
        val response = objects[0].toString()
        Logger.println("ClientConfig.PLAYER.INCOMMING.JOIN_GAME: $response")
    }

    private val mOnTickTackListener = Emitter.Listener { objects ->
        if (objects != null && objects.isNotEmpty()) {
            val data = objects[0].toString()
            if (data.isNotEmpty()) {
                val gameInfo = Gson().fromJson(data, GameInfo::class.java)
                println("game information returned: $gameInfo")
                println("mIsHandle: $mIsHandle")
                println("game information tag: ${gameInfo.tag}")
                if (gameInfo != null && mIsHandle) {
                    when (gameInfo.tag) {
                        PlayerTag.START_MOVING.value -> {
                            return@Listener
                        }
                        else -> {
                            // ========
                            if (firstTime || (timeTakingProcessing - (System.currentTimeMillis() - lastTime)) <= 0){
                                val match = gameInfo.map_info.analysisMatch()
                                val action = bomberman.nextMove(match)
                                movePlayer(action)
                                if (action in listOf("1", "2", "3", "4", "b")){
                                    timeTakingProcessing = ((35f / match.playerSpeed) * 1000).toLong() + 150
                                }
                                if (action == "x"){
                                    timeTakingProcessing = ((35f / match.playerSpeed) * 1000).toLong() + 200
                                }
                                if (action == "x"){
                                    timeTakingProcessing = ((35f / match.playerSpeed) * 1000).toLong() + 500
                                }
                                firstTime = false
                                lastTime = System.currentTimeMillis()
                            }
                        }
                    }
                }
            }
        }
    }

    private val mOnDriveStateListener = Emitter.Listener { objects ->
        val response = objects[0].toString()
        val driverInfo: Driver = Gson().fromJson(response, Driver::class.java)

        if (Constants.KEY_TEAM.startsWith(driverInfo.player_id)) {
            println("direction: ${driverInfo.direction}")
            mIsHandle = true
        }
    }

    fun connectToServer(
        connectionInfo: ConnectionInfo,
        errorCallBack: (() -> Unit)? = null,
        successCallBack: (() -> Unit)? = null
    ) {
        mSocket?.let { socket ->
            socket.disconnect()
            mSocket = null
        }
        mSocket = SocketUtils.init(connectionInfo.url, connectionInfo.isEnableProxy)
        if (mSocket == null) {
            Logger.println("Socket null - can't connect")
            return
        }
        mSocket?.on(ClientConfig.PLAYER.INCOMMING.JOIN_GAME, mOnJoinGameListener)
        mSocket?.on(ClientConfig.PLAYER.INCOMMING.TICKTACK_PLAYER, mOnTickTackListener)
        mSocket?.on(ClientConfig.PLAYER.INCOMMING.DRIVE_PLAYER, mOnDriveStateListener)
        mSocket?.on(Socket.EVENT_CONNECT) {
            Logger.println("Connected")
            successCallBack?.invoke()
            val gameParams: String = Game(connectionInfo.playId, connectionInfo.gameId).toJsonString()
            Logger.println("Game params = $gameParams")
            try {
                mSocket?.emit(ClientConfig.PLAYER.OUTGOING.JOIN_GAME, JSONObject(gameParams))
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        mSocket?.on(Socket.EVENT_CONNECT_ERROR) { objects: Array<Any> ->
            errorCallBack?.invoke()
            Logger.println(
                "Connect Failed objects = " + objects[0].toString()
            )
        }
        mSocket?.on(Socket.EVENT_DISCONNECT) {
            errorCallBack?.invoke()
            Logger.println("Disconnected")
        }
        mSocket?.connect()
    }

    fun stopGame(disconnectCallback: (() -> Unit)? = null) {
        mSocket?.on(Socket.EVENT_DISCONNECT) {
            disconnectCallback?.invoke()
            Logger.println("Disconnected")
        }
        mSocket?.disconnect()
    }

    ///==============================================================================================================
    private fun movePlayer(step: String) {
        if (mSocket != null && step.isNotEmpty()) {
            dir.setDirection(step)
            try {
                mIsHandle = false
                mSocket?.emit(ClientConfig.PLAYER.OUTGOING.DRIVE_PLAYER, JSONObject(dir.toString()))
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }
}