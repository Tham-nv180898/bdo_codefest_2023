pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }

    plugins {
        kotlin("jvm").version(extra["kotlin.version"] as String)
        id("org.jetbrains.compose").version(extra["compose.version"] as String)
        id("androidx.navigation.safeargs.kotlin") version "2.4.2" apply false
    }

}


rootProject.name = "codefest_bdo_2023"
